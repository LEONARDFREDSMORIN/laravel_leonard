<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', 'layoutController@index');

Route::get('/anggota', 'AnggotasController@index');
Route::get('/anggota/create', 'AnggotasController@create');
Route::get('/anggota/{anggota}', 'AnggotasController@show');
Route::get('/anggota/{anggota}/edit', 'AnggotasController@edit');

Route::post('/anggota', 'AnggotasController@store');
Route::put('/anggota/{anggota}', 'AnggotasController@update');
Route::delete('/anggota/{anggota}', 'AnggotasController@destroy');

Route::get('/group', 'GroupsController@index');
Route::get('/group/create', 'GroupsController@create');
Route::get('/group/{group}', 'GroupsController@show');
Route::get('/group/{group}/edit', 'GroupsController@edit');
Route::get('/userGroup/{group}', 'GroupsController@userGroup');

Route::put('/group/{group}', 'GroupsController@update');
Route::delete('/group/{group}', 'GroupsController@destroy');
Route::post('/group', 'GroupsController@store');

// Route::get('/gambar', 'UploadController@upload');
// Route::post('/upload/proses', 'UploadController@proses_upload');



