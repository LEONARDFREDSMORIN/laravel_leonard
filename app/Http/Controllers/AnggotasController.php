<?php

namespace App\Http\Controllers;

use App\Anggota;
use App\Group;
use Illuminate\Http\Request;

class AnggotasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anggota = Anggota::all();
        $group = Group::all();
        return view('anggota.anggota', compact('anggota','group'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group = Group::all();
        return view('anggota.create', compact('group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'gender' => 'required',
            'image' => 'required',
            'groupId' => 'required',
            'remarks' => 'required'
        ]);

        $file = $request->file('image');
        $nama_file = time()."_".$file->getClientOriginalName();
 
        $anggota = new Anggota;
        $anggota->name = $request->name;
        $anggota->gender = $request->gender;
        $anggota->image =  $nama_file;
        $anggota->groupId = $request->groupId;
        $anggota->remarks = $request->remarks;
        $anggota->created_at = time();

        $dirUploadImage = 'dir_image';
        $file->move($dirUploadImage,$nama_file);

        $anggota->save();
        return redirect('/anggota');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Anggota  $anggota
     * @return \Illuminate\Http\Response
     */
    public function show(Anggota $anggota)
    {
        $group = Group::all();
        $count = Anggota::where('groupId',$anggota->groupId)->get()->count();
        return view('anggota.detail', compact('anggota','count'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Anggota  $anggota
     * @return \Illuminate\Http\Response
     */
    public function edit(Anggota $anggota)
    {
        $group = Group::all();
        return view('anggota.edit', compact('anggota','group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Anggota  $anggota
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Anggota $anggota)
    {

        if ($request->image == "") {
            $anggota->image = $anggota->image;
        } else {
            $nama_file = time()."_".$request->image->getClientOriginalName();
            $dirUploadImage = 'dir_image';
            $request->image->move($dirUploadImage,$nama_file);
        }

        // dd($request->image);

        $anggota = Anggota::find($anggota->id);
        $anggota->name = $request->name;
        $anggota->gender = $request->gender;
        $anggota->groupId = $request->groupId;
        $anggota->remarks = $request->remarks; 
        $anggota->image = $nama_file;

        $anggota->save();
        return redirect('/anggota');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Anggota  $anggota
     * @return \Illuminate\Http\Response
     */
    public function destroy(Anggota $anggota)
    {
        Anggota::destroy($anggota->id);
        return redirect('/anggota');
    }
}
