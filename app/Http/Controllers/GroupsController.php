<?php

namespace App\Http\Controllers;

use App\Anggota;
use App\Group;
use Illuminate\Http\Request;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $group = Group::all(); 
        $anggota = Anggota::all();
        // dd($count);
        return view('group.group', compact('group','anggota'));
    }

    public function userGroup(Group $group)
    {
        $anggota = Anggota::where('groupId', $group->id)->get();
        return view('group.userGroup',compact('group','anggota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('group.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'remarks' => 'required',
        ]);

        $group = new Group;
        $group->name = $request->name;
        $group->remarks = $request->remarks;
        $group->created_at = time();

        $group->save();
        return redirect('/group');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        $count = Anggota::where('groupId', $group->id)->count();
        // dd($count);
        return view('group.detail', compact('group','count'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        return view('group.edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $request->validate([
            'name' => 'required',
        ]);

        Group::where('id', $group->id)
                    ->update([
                        'name' => $request->name,
                        'remarks' => $request->remarks,
                        'created_at' => $request->date
                    ]);
        return redirect('/group'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        Group::destroy($group->id);
        return redirect('/group');
    }

    
}
