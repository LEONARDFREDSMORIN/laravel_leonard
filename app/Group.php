<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'tb_group';

    public function anggota()
    {
    	return $this->belongsToMany('App\Anggota');
    }
}
