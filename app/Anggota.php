<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
    protected $table = 'tb_user';
    // protected $table = 'tb_anggota';
    // protected $fillable = ['name','gender','groupId','image', 'remarks'];

    public function group()
    {
    	return $this->belongsToMany('App\Group');
    }
}
