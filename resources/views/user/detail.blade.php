@extends('layout/masterLayout')

@section('container')

<div class="container">
	<div class="card mt-2">
	  	<div class="card-header bg-dark text-white">
	  		<h4>Detail User</h4>
	  	</div>
	  <div class="card-body">
	  	<div class="row">
		    <div class="card col-md-8 mr-4">
			  <div class="card-body">
			    <table class="table table hover table-bordered table-striped">
			    	
			    		<tr>
				    		<td>Nama</td>
				    		<td>{{$people->name}}</td>
				    	</tr>
				    	<tr>
				    		<td>Email</td>
				    		<td>{{$people->email}}</td>
				    	</tr>
				    	<tr>
				    		<td>Jenis Kelamin</td>
				    		<td>{{$people->gender}}</td>
				    	</tr>
				    	<tr>
				    		<td>Kota</td>
				    		<td>{{$people->city}}</td>
				    	</tr>
				    	<tr>
				    		<td>Alamat</td>
				    		<td>{{$people->address}}</td>
				    	</tr>
				    	    	
			    </table>
			  </div>
			</div>
			<div class="card" style="width: 18rem;">
				<div class="card-header bg-dark text-white text-center">
				  Akademik
				</div>
			  <div class="card-body text-center">
			    <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/160x160">
			  </div>
			  <ul class="list-group list-group-flush">
			    <li class="list-group-item">Dapibus ac facilisis in</li>
			    <li class="list-group-item">Vestibulum at eros</li>
			  </ul>
			  <div class="card-body">
			    <a href="#" class="card-link btn btn-primary btn-sm btn-block">View</a>
			  </div>
			</div>
	  	</div>
	  </div>
	</div>
</div>

@endsection


