@extends('layout/masterLayout')

@section('container')

<div class="container">
	<h2 class="text-center">halaman Edit</h2>
	<hr>
	<div class="col-lg-6">
		<div class="card">
		  <div class="card-body">
		    <form action="/peoples/{{$people->id}}" method="post">
		    @method('put')
			@csrf
			  <div class="form-group">
			    <label for="exampleInputEmail1">Nama</label>
			    <input type="text" name="name" class="form-control form-control-sm @error('name') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$people->name}}">
			    @error('name')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Email address</label>
			    <input type="email" name="email" class="form-control form-control-sm @error('email') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$people->email}}">
			    @error('email')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Gender</label>
			    <input type="text" name="gender" class="form-control form-control-sm @error('gender') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$people->gender}}">
			    @error('gender')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">City</label>
			    <input type="text" name="city" class="form-control form-control-sm @error('city') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$people->city}}">
			    @error('city')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Address</label>
			    <input type="text" name="address" class="form-control form-control-sm @error('address') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$people->address}}">
			    @error('address')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
		  </div>
		</div>
	</div>
</div>

@endsection


