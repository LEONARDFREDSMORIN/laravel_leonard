@extends('layout/masterLayout')

@section('container')

<div class="container">
	<h1>Daftar <small>User's</small></h1>
	<hr>
	<div class="card">
	  <div class="card-body">
	  	<a href="/peoples/create" class="btn btn-secondary btn-sm mb-2" style="float: right;">Tambah data</a>
		<table id="table_id" class="display col-lg-12">
		    <thead>
		        <tr>
		        	<th style="width: 20px;">No</th>
		            <th>Nama</th>
		            <th>Email</th>
		            <th>Alamat</th>
		            <th style="width: 100px;">Action</th>
		        </tr>
		    </thead>
		    <tbody>

		    	<?php $i=1; foreach ($peoples as $people) : ?>
			        <tr>
			        	<td><?php echo $i++; ?></td>
			            <td><a href="/peoples/{{$people->id}}">{{$people->name}}</a></td>
			            <td>{{$people->email}}</td>
			            <td>{{$people->address}}</td>
			            <td>
	                      <a href="/peoples/{{$people->id}}" class="btn btn-secondary btn-sm"><i class="fas fa-info-circle"></i></a> |
	                      <a href="/peoples/{{$people->id}}/edit" class="btn btn-secondary btn-sm"><i class="fas fa-edit"></i></a> |
	                      <form action="/peoples/{{$people->id}}" method="post" class="d-inline">
	                      	@method('delete')
	                      	@csrf
	                      <button type="submit" class="btn btn-secondary btn-sm"><i class="fas fa-trash"></i></button>
	                      </form>
	                    </td>
			        </tr>
			    <?php endforeach; ?>

		       
		    </tbody>
		</table>
	  </div>
	</div>
</div>

@endsection


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Email address</label>
		    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Password</label>
		    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
		  </div>
		  <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Save changes</button>
	      </div>
		</form>
      </div>
      
    </div>
  </div>
</div>