@extends('layout/masterLayout')

@section('container')

<div class="container">
	<h2 class="text-center">halaman tambah anggota</h2>
	<hr>
	<div class="col-lg-6">
		<div class="card">
		  <div class="card-body">
		    <form action="/anggota/{{$anggota->id}}" method="post" enctype="multipart/form-data">
		    @method('put')	
			@csrf
			  <div class="form-group">
			    <label for="exampleInputEmail1">Nama</label>
			    <input type="text" name="name" class="form-control form-control-sm @error('name') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$anggota->name}}">
			    @error('name')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			  	<label for="exampleInputEmail1">Jenis Kelamin</label><br>
			    <input class="mt-2" type="radio" name="gender" value="L">laki-laki
			    <input class="mt-2" type="radio" name="gender" value="P">Perempuan
			    
			    @error('gender')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Group</label>
			    <select class="form-control form-control" name="groupId">
			    	<?php foreach ($group as $row) : ?>
				    	<option value="{{$row->id}}">{{$row->name}}</option>
				    <?php endforeach; ?>
			    </select>
			    @error('group')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <img class="mb-3" src="{{ url('/dir_image/' . $anggota->image) }}" style="width: 150px; height: 200px;">
			  <div class="form-group">
			    <div class="custom-file">
					<input type="file" name="image">
				</div>
			    @error('image')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Catatan</label>
			    <textarea type="text" name="remarks" class="form-control form-control-sm @error('remarks') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$anggota->remarks}}"></textarea> 
			    @error('remarks')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
		  </div>
		</div>
	</div>
</div>

@endsection


