@extends('layout/masterLayout')

@section('container')

<div class="container">
	<h2 class="text-center">halaman tambah anggota</h2>
	<hr>
	<div class="col-lg-6">
		<!-- pesan error -->
			@if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                 @foreach ($errors->all() as $error)
                 	<li>{{ $error }}</li>
                 @endforeach
                </ul>
            </div>
            @endif
		<div class="card">

		  <div class="card-body">
		    <form action="/anggota" method="post" enctype="multipart/form-data">
			@csrf
			  <div class="form-group">
			    <label for="exampleInputEmail1">Nama</label>
			    <input type="text" name="name" class="form-control form-control-sm @error('name') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{old('name')}}">
			    @error('name')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			  	<label for="exampleInputEmail1">Jenis Kelamin</label><br>
			    <input class="mr-2" type="radio" name="gender" value="L">laki-laki 
			    <br>
			    <input class="mr-2" type="radio" name="gender" value="P">Perempuan
			    
			    @error('gender')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Group</label>
			    <select class="form-control form-control" name="groupId">
			    	<?php foreach ($group as $row) : ?>
				    	<option value="{{$row->id}}">{{$row->name}}</option>
				    <?php endforeach; ?>
			    </select>
			    @error('group')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			  	<label for="exampleInputEmail1">Gambar</label>
			    <div class="custom-file">
					<input type="file" name="image">
				</div>
			    @error('image')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Catatan</label>
			    <textarea type="text" name="remarks" class="form-control form-control-sm @error('remarks') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{old('remarks')}}"></textarea>
			    @error('remarks')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
		  </div>
		</div>
	</div>
</div>

@endsection


