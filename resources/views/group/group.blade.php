@extends('layout/masterLayout')

@section('container')

<div class="container">
	<h1>Daftar <small>Group</small></h1>
	<hr>
	<div class="card">
	  <div class="card-body">
	  	<a href="/group/create" class="btn btn-secondary btn-sm mb-2" style="float: right;">Tambah Group</a>
		<table id="table_id" class="display col-lg-12">
		    <thead>
		        <tr>
		        	<th class="user-tbl" style="width: 20px;">No</th>
		        	<th class="user-tbl">Kode Group</th>
		            <th class="user-tbl">Nama Group</th>
		            <th class="user-tbl">Catatan</th>
		            <th class="user-tbl">Jumlah User</th>
		        </tr>
		    </thead>
		    <tbody>

		    	
		    	<?php $i=1; foreach ($group as $row) : ?>
			    	<tr>
			    		<td class="user-tbl"><?php echo $i++; ?></td>
			    		<td class="user-tbl">GR-{{$row->id}}</td>
			    		<td class="user-tbl"><a href="/group/{{$row->id}}">{{$row->name}}</a></td>
			    		<td class="user-tbl">{{$row->remarks}}</td>
				    	<td class="user-tbl" ><a style="width: 120px;" class="btn btn-dark btn-sm" href="/userGroup/{{$row->id}}">view</a></td>
			    	</tr>
			    <?php endforeach; ?>

		       
		    </tbody>
		</table>
	  </div>
	</div>
</div>

@endsection