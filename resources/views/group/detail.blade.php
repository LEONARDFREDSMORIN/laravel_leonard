@extends('layout/masterLayout')

@section('container')

<div class="container">
	<div class="card mt-2">
	  	<div class="card-header bg-dark text-white">
	  		<h4>Detail Group</h4>
	  	</div>
	  <div class="card-body">
	  	<div class="row">
		    <div class="card col-md-6">
			  <div class="card-body">
			    <table class="table table hover table-bordered table-striped">
			    		<tr>
				    		<td>Nama</td>
				    		<td>{{$group->name}}</td>
				    	</tr>
				    	<tr>
				    		<td>Jumlah User</td>
				    		<td>{{$count}} Anggota</td>
				    	</tr>
				    	<tr>
				    		<td>Tanggal dibuat</td>
				    		<td>{{$group->created_at}}</td>
				    	</tr>
			    </table>

			    <a href="/group/{{$group->id}}/edit" class="btn btn-success btn-sm mt-3 mr-2">Edit Group</a> 
	            <form action="/group/{{$group->id}}" method="post" class="d-inline">
	            @method('delete')
	            @csrf
	          	   <button type="submit" class="btn btn-danger btn-sm mt-3">Hapus</button>
	            </form>
			  </div>
			</div>
	  	</div>
	  </div>
	</div>
</div>

@endsection


