@extends('layout/masterLayout')

@section('container')

<div class="container">
	<h2 class="text-center">halaman tambah gruop</h2>
	<hr>
	<div class="col-lg-6">
		<div class="card">
		  <div class="card-body">
		    <form action="/group" method="post">
			@csrf
			  <div class="form-group">
			    <label for="exampleInputEmail1">Nama Group</label>
			    <input type="text" name="name" class="form-control form-control-sm @error('name') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{old('name')}}">
			    @error('name')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Catatan Group</label>
			    <textarea type="text" name="remarks" class="form-control form-control-sm @error('remarks') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{old('remarks')}}"></textarea> 
			    @error('remarks')
			   	 <div class="invalid-feedback">{{$message}}</div>
			    @enderror
			  </div>
		  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
		  </div>
		</div>
	</div>
</div>

@endsection


