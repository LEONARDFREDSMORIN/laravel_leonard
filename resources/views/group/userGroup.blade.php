@extends('layout/masterLayout')

@section('container')

<div class="container">
	<h1>Daftar <small>Group</small></h1>
	<hr>
	<div class="card">
	  <div class="card-body">
		<table id="table_id" class="display col-lg-12">
		    <thead>
		        <tr>
		        	<th style="width: 20px;">No</th>
		        	<th class="user-tbl">Nama Anggota</th>
		            <th class="user-tbl">Jenis Kelamin</th>
		            <th class="user-tbl">group</th>
		        </tr>
		    </thead>
		    <tbody>

		    <?php $i=1; foreach ($anggota as $row) : ?>	
			    <tr>
					<td class="user-tbl"><?php echo $i++; ?></td>
					<td class="user-tbl">{{$row->name}}</td>
					<td class="user-tbl">{{$row->gender}}</td>
					<td class="user-tbl">{{$row->groupId}}</td>
				</tr>
			<?php endforeach; ?>			
		       
		    </tbody>
		</table>
	  </div>
	</div>
</div>

@endsection